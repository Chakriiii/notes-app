package com.example.todoroom

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import java.text.SimpleDateFormat
import java.util.*

class NewActivity : AppCompatActivity() {

    private lateinit var noteTitleEdt: TextView
    private lateinit var noteDescriptionEdt: TextView
    private lateinit var addUpdateBtn: Button
    private lateinit var viewModel: ViewModel
    private var noteID = 0


    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new)

        noteTitleEdt = findViewById(R.id.idEdtNoteTitle)
        noteDescriptionEdt = findViewById(R.id.idEdtNoteDescription)
        addUpdateBtn = findViewById(R.id.idBtnAddUpdate)
        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.AndroidViewModelFactory.getInstance(application)
        ).get(ViewModel::class.java)

        val noteType = intent.getStringExtra("noteType")
        if (noteType.equals("Edit")) {
            val noteTitle = intent.getStringExtra("noteTitle")
            val noteDesc = intent.getStringExtra("noteDescription")
            noteID = intent.getIntExtra("noteID", 0)
            addUpdateBtn.text = "Update Note"
            noteTitleEdt.text = noteTitle
            noteDescriptionEdt.text = noteDesc
        } else {
            addUpdateBtn.text = "Save Note"
        }

        addUpdateBtn.setOnClickListener {
            val noteTitle = noteTitleEdt.text.toString()
            val notedescription = noteDescriptionEdt.text.toString()

            if (noteType.equals("Edit")) {
                if (noteTitle.isNotEmpty() && notedescription.isNotEmpty()) {
                    val sdf = SimpleDateFormat("dd-MM-yyyy - HH:mm")
                    val currentDate: String = sdf.format(Date())
                    val updateNote = Note(noteTitle, notedescription, currentDate)
                    updateNote.id = noteID
                    viewModel.updateNote(updateNote)
                }
            } else {
                if (noteTitle.isNotEmpty() && notedescription.isNotEmpty()) {
                    val sdf = SimpleDateFormat("dd-MM-yyyy - HH:mm")
                    val currentDate: String = sdf.format(Date())
                    viewModel.addNote(Note(noteTitle, notedescription, currentDate))
                }
            }
            startActivity(Intent(applicationContext, MainActivity::class.java))
            this.finish()
        }
    }
}